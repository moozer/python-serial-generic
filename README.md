This is a minimal example of how to use UART with python.

It shows

* Receiving characters over serial: `serial_receive.py`
* Sending and receiving characters over serial: `simple_connection.py`
* Testing using gitlab CI

The specific serial device to be used is hardcoded in the python files. To change either the baud rate or the device, edit the files and change the "serial_cfg" structure.


Usage
----------

First set up virtual environment and install required pip packages

```
virtualenv -p python3 venv
source venv/bin/activate
pip3 install -r requirements.txt
```

Set up the pipes
```
cd test
socat -d -d pty,raw,echo=0,link=./fake_device pty,raw,echo=0,link=client &
```


## Test recieve example

In terminal 1, run
```
./fake_device_A.py
```

In terminal 2, run
```
python3 simple_connection.py
```


## Test send/receive example

In terminal 1, run
```
./fake_device_B.py
```

In terminal 2, run
```
python3 serial_receive.py
```
