#!/usr/bin/env python3
import serial
import time

serial_cfg = {  "dev": "./fake_device",
                "baud": 9600 }


if __name__ == "__main__":
    print( "Running port {}".format( serial_cfg['dev'] ) )

    with serial.Serial(serial_cfg['dev'], serial_cfg['baud']) as ser:
        try:
            count = 0
            while( True):
                print( "sending: {}".format(count))
                ser.write( "{}\r\n".format(count).encode())
                count = count +1
                time.sleep(1)
        except KeyboardInterrupt:
            print('interrupted!')
