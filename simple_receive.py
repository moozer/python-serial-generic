#!/usr/bin/env python3

import serial
import time

serial_cfg = {  "dev": "test/client",
                "baud": 9600 }

max_run_count = 5

if __name__ == "__main__":
    print( "Running port {}".format( serial_cfg['dev'] ) )
    print( "- will output line-by-line, separated by newline char")

    with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=2) as ser:
        try:
            run_count = 0
            while True:
                reply = ser.readline().decode()   # read a '\n' terminated line
                if len( reply ) > 0:
                    print( "- {}".format(reply.strip()))
                else:
                    print( "- nothing received. Serial timeout?")

                run_count = run_count +1
                if max_run_count  > 0 and run_count >= max_run_count:
                    print( "reached max number of loops")
                    break

        except KeyboardInterrupt:
            print('interrupted!')
